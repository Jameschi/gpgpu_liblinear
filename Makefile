CXX ?= g++
CC ?= gcc
CUDACC ?= nvcc
CFLAGS = -Wall -Wconversion -O3 -fPIC -fopenmp
CXXFLAGS = -std=c++11 $(CFLAGS)
# CFLAGS += -DCV_OMP
LIBS = blas/blas.a
SHVER = 3
OS = $(shell uname)
#LIBS = -lblas
CUDALIBS = -lcublas -lcusparse

ifdef CUACCL
CXXFLAGS += -DCUACCL
endif

all: train predict

lib: cuda/profile.o linear.o tron.o blas/blas.a
	if [ "$(OS)" = "Darwin" ]; then \
		SHARED_LIB_FLAG="-dynamiclib -Wl,-install_name,liblinear.so.$(SHVER)"; \
	else \
		SHARED_LIB_FLAG="-shared -Wl,-soname,liblinear.so.$(SHVER)"; \
	fi; \
	$(CXX) -fopenmp $${SHARED_LIB_FLAG} cuda/profile.o linear.o tron.o blas/blas.a -o liblinear.so.$(SHVER)

ifdef CUACCL
train: cuda/profile.o cuda/cuaccl.o train.o tron.o linear.o train.c blas/blas.a
	$(CUDACC) -std=c++11 -O3 -Xcompiler -fopenmp -o train cuda/profile.o cuda/cuaccl.o train.o tron.o linear.o $(LIBS) $(CUDALIBS)

predict: cuda/profile.o tron.cpp tron.h linear.cpp linear.h predict.c blas/blas.a
	$(CXX) -std=c++11 $(CFLAGS) -o predict predict.c tron.cpp linear.cpp cuda/profile.o $(LIBS)
else
train: cuda/profile.o train.o tron.o linear.o train.c blas/blas.a
	$(CXX) $(CXXFLAGS) -o train train.c cuda/profile.o tron.o linear.o $(LIBS)

predict: cuda/profile.o tron.o linear.o predict.c blas/blas.a
	$(CXX) $(CFLAGS) -o predict predict.c cuda/profile.o tron.o linear.o $(LIBS)
endif

cuda/profile.o: cuda/profile.cpp
	$(CXX) $(CXXFLAGS) -c -o cuda/profile.o cuda/profile.cpp

cuda/cuaccl.o: cuda/cuaccl.cu
	$(CUDACC) -std=c++11 -arch sm_30 -O3 -D_FORCE_INLINES -Xcompiler -Wall -Xcompiler -Wextra -Xcompiler -Wno-unused-parameter -Xcompiler -fPIC -Xcompiler -fopenmp -c -o cuda/cuaccl.o cuda/cuaccl.cu

train.o: train.c
	$(CXX) $(CXXFLAGS) -c -o train.o train.c

tron.o: tron.cpp tron.h
	$(CXX) $(CXXFLAGS) -c -o tron.o tron.cpp

linear.o: linear.cpp linear.h
	$(CXX) $(CXXFLAGS) -c -o linear.o linear.cpp

blas/blas.a: blas/*.c blas/*.h
	make -C blas OPTFLAGS='$(CFLAGS)' CC='$(CC)';

clean:
	make -C blas clean
	make -C matlab clean
	rm -f *~ tron.o linear.o train predict liblinear.so.$(SHVER)
	rm -f cuda/*.o
