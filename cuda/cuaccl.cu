#include "cuaccl.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <vector>
#include <algorithm>
#include <cusparse_v2.h>
#include <cublas_v2.h>
#include <thrust/reduce.h>
#include <thrust/device_vector.h>

static const int TPB = 256;
static const int PN = 4;
static cusparseHandle_t hSparse, hmSparse[PN];
static cudaStream_t mStream[PN];
static cublasHandle_t hBlas;

static int *feature_count;
static int *feature_count_T;

class CSR {
    private:
        const int m, n;
        double *d_val;
        int *d_rowptr;
        int *d_colidx;
        size_t nz;
        cusparseMatDescr_t desc;

    public:
        CSR(feature_node **x, const int _m, const int _n) : m(_m), n(_n) {
            double *val;
            int *rowptr;
            int *colidx;

            nz = 0;
            rowptr = new int[m + 1];
            for(int i = 0; i < m; i++) {
                rowptr[i] = nz;
                nz += feature_count[i];
            }

            val = new double[nz];
            colidx = new int[nz];
            int i;
#pragma omp parallel for private (i) schedule(static)
            for(i = 0; i < m; i++) {
                off_t off = rowptr[i];
                const feature_node *xi = x[i];
                for(int j = 0; xi[j].index != -1; j++) {
                    val[off] = xi[j].value;
                    // One base to zero base.
                    colidx[off] = xi[j].index - 1;
                    off += 1;
                }
            }
            rowptr[m] = nz;

            cudaMalloc(&d_val, sizeof(*d_val) * nz);
            cudaMalloc(&d_rowptr, sizeof(*d_rowptr) * (m + 1));
            cudaMalloc(&d_colidx, sizeof(*d_colidx) * nz);
            cudaMemcpy(d_val, val, sizeof(*d_val) * nz,
                cudaMemcpyHostToDevice);
            cudaMemcpy(d_rowptr, rowptr, sizeof(*d_rowptr) * (m + 1),
                cudaMemcpyHostToDevice);
            cudaMemcpy(d_colidx, colidx, sizeof(*d_colidx) * nz,
                cudaMemcpyHostToDevice);
            cusparseCreateMatDescr(&desc);
            cusparseSetMatType(desc, CUSPARSE_MATRIX_TYPE_GENERAL);
            cusparseSetMatIndexBase(desc, CUSPARSE_INDEX_BASE_ZERO);

            delete[] val;
            delete[] rowptr;
            delete[] colidx;
        }
        ~CSR() {
            cudaFree(d_val);
            cudaFree(d_rowptr);
            cudaFree(d_colidx);
        }
        int mul(const double *src, double *dst) {
            double alpha = 1, beta = 0;
            cusparseDcsrmv(hSparse, CUSPARSE_OPERATION_NON_TRANSPOSE,
                m, n, nz, &alpha, desc,
                d_val, d_rowptr, d_colidx,
                src, &beta, dst);
            cudaDeviceSynchronize();
            return 0;
        }

        CSR& operator=(const CSR &other) = delete;
        CSR(const CSR &other) = delete;
        CSR(CSR &&other) = delete;
        CSR& operator=(CSR &&other) = delete;
};

__global__ void krn_sumvec(double *src, const int n) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
    double val = 0;
    if (idx < n) {
        for(int i = 0; i < PN; i++) {
            val += src[idx + n * i];
        }
        src[idx] = val;
    }
}
class TCSR {
    private:
        int part, ori_m, sub_m[2], total_m;

        double *d_ts_val;
        int *d_ts_rowptr;
        int *d_ts_colidx;
        size_t ts_nz;

        double *d_val[2][PN];
        int *d_rowptr[2][PN];
        int *d_colidx[2][PN];
        size_t nz[2][PN];

        cusparseMatDescr_t desc;
        double *d_imd;

        static void init_matrix(const std::vector<double> &val,
                const std::vector<int> &rowptr, const std::vector<int> &colidx,
                double **d_vl, int **d_rp, int **d_ci
        ) {
            assert(colidx.size() == val.size());

            cudaMalloc(d_vl, sizeof(**d_vl) * val.size());
            cudaMalloc(d_rp, sizeof(**d_rp) * rowptr.size());
            cudaMalloc(d_ci, sizeof(**d_ci) * colidx.size());
            cudaMemcpy(*d_vl, &val[0], sizeof(**d_vl) * val.size(),
                    cudaMemcpyHostToDevice);
            cudaMemcpy(*d_rp, &rowptr[0], sizeof(**d_rp) * rowptr.size(),
                    cudaMemcpyHostToDevice);
            cudaMemcpy(*d_ci, &colidx[0], sizeof(**d_ci) * colidx.size(),
                    cudaMemcpyHostToDevice);
        }

    public:
        TCSR(feature_node **x, const int m, const int n)
            : part((n + PN - 1) / PN), ori_m(m)
        {
            int i;
            std::pair<int, double> **tmp;
            int use;

            std::vector<double> ts_val;
            std::vector<int> ts_rowptr;
            std::vector<int> ts_colidx;

            std::vector<double> val[2][PN];
            std::vector<int> rowptr[2][PN];
            std::vector<int> colidx[2][PN];

            tmp = new std::pair<int, double>*[m];
            for(int i = 0; i < m; i++) {
                tmp[i] = new std::pair<int, double>[feature_count_T[i]];
            }
            memset(feature_count_T, 0, sizeof(*feature_count_T) * m);
            for(i = 0; i < n; i++) {
                feature_node *xi = x[i];
                for(int j = 0; j < feature_count[i]; j++) {
                    const int k = xi[j].index - 1;
                    const int val = feature_count_T[k]++;
                    tmp[k][val].first = i;
                    tmp[k][val].second = xi[j].value;
                }
            }

            {
                TP tp("split");
            total_m = 0;
            use = 0;
            for(int i = 0; i < m; i++) {
                ts_rowptr.push_back(ts_val.size());

                auto *rv = tmp[i];
                if(feature_count_T[i] < n / 10000) {
                    assert(use == 0 && "Features are out of order.");
                    use = 0;
                } else {
                    use = 1;
                }
                auto &vl = val[use];
                auto &rp = rowptr[use];
                auto &ci = colidx[use];

                for(size_t j = 0; j < feature_count_T[i];) {
                    ts_colidx.push_back(total_m);
                    ts_val.push_back(1.0);
                    total_m += 1;

                    for(int k = 0; k < PN; k++) {
                        rp[k].push_back(vl[k].size());
                    }
                    for(int k = 0; k < 2048 && j < feature_count_T[i]; k++, j++) {
                        int buck = rv[j].first / part;
                        ci[buck].push_back(rv[j].first - buck * part);
                        vl[buck].push_back(rv[j].second);
                    }
                }

                // Free memory immediately.
                delete[] rv;
            }
            delete[] tmp;
            }

            ts_nz = ts_val.size();
            ts_rowptr.push_back(ts_nz);

            for(int use = 0; use < 2; use++) {
                // rowptr[][0] = rowptr[][1] = rowptr[][2] ...
                sub_m[use] = rowptr[use][0].size();
                for(int i = 0; i < PN; i++) {
                    nz[use][i] = val[use][i].size();
                    rowptr[use][i].push_back(nz[use][i]);

                    printf("\t%d %d %lu\n", use, sub_m[use], nz[use][i]);
                }
            }

            init_matrix(ts_val, ts_rowptr, ts_colidx,
                    &d_ts_val, &d_ts_rowptr, &d_ts_colidx);
            for(int use = 0; use < 2; use++) {
                for(int i = 0; i < PN; i++) {
                    init_matrix(val[use][i], rowptr[use][i], colidx[use][i],
                            &d_val[use][i],
                            &d_rowptr[use][i],
                            &d_colidx[use][i]);
                }
            }

            cusparseCreateMatDescr(&desc);
            cusparseSetMatType(desc, CUSPARSE_MATRIX_TYPE_GENERAL);
            cusparseSetMatIndexBase(desc, CUSPARSE_INDEX_BASE_ZERO);

            cudaMalloc(&d_imd, sizeof(*d_imd) * total_m * PN);
        }
        ~TCSR() {
            cudaFree(d_ts_val);
            cudaFree(d_ts_rowptr);
            cudaFree(d_ts_colidx);
            for(int use = 0; use < 2; use++) {
                for(int i = 0; i < PN; i++) {
                    cudaFree(d_val[use][i]);
                    cudaFree(d_rowptr[use][i]);
                    cudaFree(d_colidx[use][i]);
                }
            }
            cudaFree(d_imd);
        }
        int mul(const double *src, double *dst) {
            double alpha = 1, beta = 0;

            double *d_base = d_imd;
            for(int use = 0; use < 2; ++use) {
                for(size_t i = 0; i < PN; ++i) {
                    cusparseDcsrmv(hmSparse[i],
                            CUSPARSE_OPERATION_NON_TRANSPOSE,
                            sub_m[use], part, nz[use][i], &alpha, desc,
                            d_val[use][i], d_rowptr[use][i], d_colidx[use][i],
                            src + i * part, &beta, d_base + total_m * i);
                }
                d_base = d_base + sub_m[use];
            }
            cudaDeviceSynchronize();

            krn_sumvec<<<(total_m + TPB - 1) / TPB, TPB>>>(d_imd, total_m);
            cudaDeviceSynchronize();

            cusparseDcsrmv(hSparse, CUSPARSE_OPERATION_NON_TRANSPOSE,
                    ori_m, total_m, ts_nz, &alpha, desc,
                    d_ts_val, d_ts_rowptr, d_ts_colidx,
                    d_imd, &beta, dst);
            cudaDeviceSynchronize();
            return 0;
        }

        TCSR& operator=(const TCSR &other) = delete;
        TCSR(const TCSR &other) = delete;
        TCSR(TCSR &&other) = delete;
        TCSR& operator=(TCSR &&other) = delete;
};

static int *feature_order;

int cuaccl_init() {
    assert(cudaSetDeviceFlags(cudaDeviceScheduleAuto | cudaDeviceMapHost)
        == cudaSuccess);
    assert(cusparseCreate(&hSparse) == cudaSuccess);
    for(int i = 0; i < PN; i++) {
        assert(cudaStreamCreate(&mStream[i]) == cudaSuccess);
        assert(cusparseCreate(&hmSparse[i]) == cudaSuccess);
        cusparseSetStream(hmSparse[i], mStream[i]);
    }

    assert(cublasCreate(&hBlas) == cudaSuccess);

    printf("cuAccl init\n");
    return 0;
}
int cuaccl_destroy() {
    cusparseDestroy(hSparse);
    for(int i = 0; i < PN; i++) {
        cusparseDestroy(hmSparse[i]);
        cudaStreamDestroy(mStream[i]);
    }
    cublasDestroy(hBlas);

    delete[] feature_order;
    delete[] feature_count;
    delete[] feature_count_T;

    printf("cuAccl destroy\n");
    return 0;
}
int cuaccl_preprocess(
    feature_node **x,
    double *y,
    const int l,
    const int n,
    const bool random
) {
    int **tmp;
    int *order;
    int *remap;
    int *count;
    int *count_T;
    int i;

    if(random) {
        for(i = 0; i < l; i++) {
            int j = i + rand() % (l - i);
            feature_node *swpx = x[i];
            double swpy = y[i];
            x[i] = x[j];
            x[j] = swpx;
            y[i] = y[j];
            y[j] = swpy;
        }
    }
    
    tmp = new int*[n];
    order = new int[n];
    remap = new int[n];
    count = new int[l];
    count_T = new int[n];
    feature_count = new int[l];
    feature_count_T = new int[n];

    // Reorder features.
    for(i = 0; i < n; i++) {
        order[i] = i;
        remap[i] = -1;
    }
    memset(count, 0, sizeof(*count) * l);
    memset(count_T, 0, sizeof(*count_T) * n);
    for(i = 0; i < l; i++) {
        for(int j = 0; x[i][j].index != -1; j++) {
            count[i] += 1;
            count_T[x[i][j].index - 1] += 1;
        }
    }
    for(i = 0; i < n; i++) {
        tmp[i] = new int[count_T[i]];
    }
    memset(count_T, 0, sizeof(*count_T) * n);
#pragma omp parallel for private (i) schedule(static)
    for(i = 0; i < l; i++) {
        for(int j = 0; x[i][j].index != -1; j++) {
            const int k = x[i][j].index - 1;
            int val = __atomic_fetch_add(&count_T[k], 1,
                __ATOMIC_SEQ_CST);
            tmp[k][val] = i; 
        }
    }
    std::sort(order, order + n,
            [&](
                const int &a,
                const int &b
            ) {
                return count_T[a] < count_T[b];
            });
    for(i = 0; i < n; i++) {
        remap[order[i]] = i;
        feature_count_T[i] = count_T[order[i]];
    }

    feature_order = order;
    delete[] count_T;

#pragma omp parallel for private (i) schedule(static)
    for(i = 0; i < l; i++) {
        int j;
        for(j = 0; x[i][j].index != -1; j++) {
            x[i][j].index = remap[x[i][j].index - 1] + 1;
        }
        std::sort(x[i], x[i] + j,
                [&](const feature_node &a, const feature_node b) {
                    return a.index < b.index;
                });
    }
    delete[] remap;

    // Algnment data.
    const int part = (l + PN - 1) / PN;
    int idx[PN];
    feature_node **nx;
    double *ny;

    nx = new feature_node*[l];
    ny = new double[l];
    for(int k = 0; k < PN; k++) {
        idx[k] = part * k;
    }
    for(i = n - 1; i >= 0; i--) {
        const int *row = tmp[order[i]];
        for(int j = 0; j < feature_count_T[i]; j++) {
            if(x[row[j]] != NULL) {
                int t = idx[row[j] / part];
                idx[row[j] / part] += 1;
                nx[t] = x[row[j]];
                ny[t] = y[row[j]];
                feature_count[t] = count[row[j]];
                x[row[j]] = NULL;
            }
        }
    }

    // Fix empty data.
    for(i = 0; i < l; i++) {
        if(x[i] != NULL) {
            int t = idx[i / part];
            idx[i / part] += 1;
            nx[t] = x[i];
            ny[t] = y[i];
            feature_count[t] = count[i];
            x[i] = NULL;
        }
    }
    memcpy(x, nx, sizeof(*x) * l);
    memcpy(y, ny, sizeof(*y) * l);
    delete[] nx;
    delete[] ny;

    for(i = 0; i < n; i++) {
        delete[] tmp[i];
    }
    delete[] tmp;
    delete[] count;
    return 0;
}
int cuaccl_orderback(double *w, const int n) {
    int i;
    double *nw = new double[n];
#pragma omp parallel for private (i) schedule(guided)
    for(i = 0; i < n; i++) {
        nw[feature_order[i]] = w[i];
    }
    memcpy(w, nw, sizeof(*w) * n);
    delete[] nw;
    return 0;
}

namespace cuaccl_l2r_lr {
static feature_node **full_x;
static double *full_y;
static int l, n;
__constant__ int c_l;
static CSR *X;
static TCSR *Xt;
static double *d_D;
static double *d_C;
static double *d_L;
static double *h_y, *d_y;
static double *d_z; // Become internal variable.
static double *d_ltmp;
static double *d_w;
static double *d_g;

static int mask_begin;
__constant__ int c_mask_begin;
static int mask_end;
__constant__ int c_mask_end;

int init(
    feature_node **x,
    double *y,
    const int _l,
    const int _n
) {
    full_x = x;
    full_y = y;

    l = _l;
    n = _n;
    cudaMemcpyToSymbol(c_l, &l, sizeof(c_l));

    {
        TP tp("X");
    X = new CSR(x, l, n);
    }
    {
        TP tp("Xt");
    Xt = new TCSR(x, n, l);
    }

    // Lazy y label.
    cudaHostAlloc(&h_y, sizeof(*d_y) * l,
        cudaHostAllocMapped | cudaHostAllocWriteCombined);
    cudaHostGetDevicePointer(&d_y, h_y, 0);

    cudaMalloc(&d_C, sizeof(*d_C) * l);
    cudaMalloc(&d_D, sizeof(*d_D) * l);
    cudaMalloc(&d_L, sizeof(*d_L) * l);
    cudaMalloc(&d_z, sizeof(*d_z) * l);
    cudaMalloc(&d_ltmp, sizeof(*d_ltmp) * l);
    cudaMalloc(&d_w, sizeof(*d_w) * n);
    cudaMalloc(&d_g, sizeof(*d_g) * n);
    return 0;
}
int destroy() {
    delete X;
    delete Xt;
    cudaFreeHost(h_y);
    cudaFree(d_C);
    cudaFree(d_D);
    cudaFree(d_L);
    cudaFree(d_z);
    cudaFree(d_ltmp);
    cudaFree(d_w);
    cudaFree(d_g);
    return 0;
}
int lazy_label(int *label) {
    int i;

#pragma omp parallel for private (i) schedule(guided)
    for(i = 0; i < l; i++) {
        if(full_y[i] == label[0]) {
            h_y[i] = +1;
        } else {
            h_y[i] = -1;
        }
    }

    return 0;
}
int create(feature_node **sub_x, const double *C) {
    mask_begin = -1;
    mask_end = -1;

    for(int i = 0, j = 0; i < l; i++) {
        if(full_x[i] == sub_x[j]) {
            j++;
            if(mask_begin != -1) {
                mask_end = i;
                break;
            }
        } else {
            if(mask_begin == -1) {
                mask_begin = i;
                mask_end = l;
            }
        }
    }
    cudaMemcpyToSymbol(c_mask_begin, &mask_begin, sizeof(c_mask_begin));
    cudaMemcpyToSymbol(c_mask_end, &mask_end, sizeof(c_mask_end));

    if(mask_begin == -1) {
        // Full problem.
        cudaMemcpy(d_C, C, sizeof(*d_C) * l, cudaMemcpyHostToDevice);
    } else {
        // Sub problem.
        cudaMemcpy(d_C, C, sizeof(*d_C) * mask_begin, cudaMemcpyHostToDevice);
        cudaMemset(d_C + mask_begin, 0, sizeof(*d_C) * (mask_end - mask_begin));
        cudaMemcpy(d_C + mask_end, C + mask_begin,
            sizeof(*d_C) * (l - mask_end), cudaMemcpyHostToDevice);
    }
    return 0;
}

__global__ void krn_mulvec(
    const double *src,
    const double *a,
    const double *b,
    double *dst
) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < c_l) {
        // a(d_C) has been masked to zero, no need to mask again.
        dst[idx] = src[idx] * a[idx] * b[idx];
    }
}
int Hv(const double *s, double *Hs) {
{
    TP tp("Hv_1");
    X->mul(s, d_L);
}
{
    TP tp("Hv_D");
    krn_mulvec<<<(l + TPB - 1) / TPB, TPB>>>(d_L, d_C, d_D, d_L);
    cudaDeviceSynchronize();
}
{
    TP tp("Hv_2");
    Xt->mul(d_L, Hs);
}
    return 0;
}
int Xv(const double *v, double *Xv) {
    TP tp("Xv");
    X->mul(v, Xv);
    return 0;
}
int XTv(const double *v, double *XTv) {
    TP tp("XTv");
    Xt->mul(v, XTv);
    return 0;
}
__global__ void krn_fun_support(
    const double *y_d,
    const double *C_d,
    const double *z_d,
    const int size,
    double *ltmp_d
) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if(idx < size) {
        if(idx >= c_mask_begin && idx < c_mask_end) {
            ltmp_d[idx] = 0;
        } else {
            double yz = y_d[idx] * z_d[idx];
            double val = C_d[idx];
            if (yz >= 0) {
                ltmp_d[idx] = val * log(1 + exp(-yz));
            } else {
                ltmp_d[idx] = val * (-yz + log(1 + exp(yz)));
            }
        }
    }
}
__global__ void krn_grad_support_pre(
    double *D_d,
    double *z_d,
    const double *y_d,
    const double *C_d,
    const int size
) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if(idx < size) {
        if(idx >= c_mask_begin && idx < c_mask_end) {
            D_d[idx] = 0;
            z_d[idx] = 0;
        } else {
            double val = 1.0 / (1 + exp(-y_d[idx] * z_d[idx]));
            D_d[idx] = val * (1 - val);
            z_d[idx] = C_d[idx] * (val - 1) * y_d[idx];
        }
    }
}
__global__ void krn_grad_support_post(
    const double *w_d,
    double *g_d,
    int size
) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if(idx < size) {
        g_d[idx] = w_d[idx] + g_d[idx];
    }
}
double fun(const double *w) {
    const int inc = 1;
    double f;

    cudaMemcpy(d_w, w, sizeof(double) * n, cudaMemcpyHostToDevice);

    Xv(d_w, d_z);

    cublasDdot(hBlas, n, d_w, inc, d_w, inc, &f);
    f /= 2.0;

    krn_fun_support<<<(l + TPB - 1) / TPB, TPB>>>(d_y, d_C, d_z, l, d_ltmp);
    cudaDeviceSynchronize();

    thrust::device_ptr<double> ltmp_devptr(d_ltmp);
    f = thrust::reduce(ltmp_devptr, ltmp_devptr + l, f);

    return f;
}
int grad(const double *w, double *g) {
    cudaMemcpy(d_w, w, sizeof(double) * n, cudaMemcpyHostToDevice);

    krn_grad_support_pre<<<(l + TPB - 1) / TPB, TPB>>>(d_D, d_z, d_y, d_C, l);
    cudaDeviceSynchronize();

    XTv(d_z, d_g);

    krn_grad_support_post<<<(n + TPB - 1) / TPB, TPB>>>(d_w, d_g, n);
    cudaDeviceSynchronize();

    cudaMemcpy(g, d_g, sizeof(double) * n, cudaMemcpyDeviceToHost);
    return 0;
}
} // End namespace cuaccl_l2r_lr.

namespace cuaccl_trcg {
static function *fun_obj;
static int n;
__constant__ int c_n;
static double *d, *Hd, *g, *s, *r;

int init(const int _n) {
    n = _n;
    cudaMemcpyToSymbol(c_n, &n, sizeof(c_n));
    cudaMalloc(&d, sizeof(double) * n);
    cudaMalloc(&Hd, sizeof(double) * n);
    cudaMalloc(&g, sizeof(double) * n);
    cudaMalloc(&s, sizeof(double) * n);
    cudaMalloc(&r, sizeof(double) * n);
    return 0;
}
int destroy() {
    cudaFree(d);
    cudaFree(Hd);
    cudaFree(g);
    cudaFree(s);
    cudaFree(r);
    return 0;
}
int create(function *_fun_obj) {
    fun_obj = _fun_obj;
    return 0;
}
__global__ void krn_initval(const double *g, double *s, double *r, double *d) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if(idx < c_n) {
        s[idx] = 0.f;
        r[idx] = -g[idx];
        d[idx] = r[idx];
    }
}
int trcg(
    const double delta,
    const double *cpu_g,
    double *cpu_s,
    double *cpu_r,
    const double eps_cg
) {
    const int inc = 1;
    const double one = 1;
    double rTr, rnewTrnew, alpha, beta, cgtol, res_dnrm2, res_ddot;
    int cg_iter = 0;

    cudaMemcpy(g, cpu_g, sizeof(double) * n, cudaMemcpyHostToDevice);

    krn_initval<<<(n + TPB - 1) / TPB, TPB>>>(g, s, r, d);
    cudaDeviceSynchronize();

    cublasDnrm2(hBlas, n, g, inc, &res_dnrm2);
    cgtol = eps_cg * res_dnrm2;

    cublasDdot(hBlas, n, r, inc, r, inc, &rTr);
    while(true) {
        cublasDnrm2(hBlas, n, r, inc, &res_dnrm2);
        if(res_dnrm2 <= cgtol) {
            break;
        }
        cg_iter++;

        fun_obj->Hv(d, Hd);
            
        cublasDdot(hBlas, n, d, inc, Hd, inc, &res_ddot);
        alpha = rTr / res_ddot;
        cublasDaxpy(hBlas, n, &alpha, d, inc, s, inc);
            
        cublasDnrm2(hBlas, n, s, inc, &res_dnrm2);
        if(res_dnrm2 > delta) {
            printf("cg reaches trust region boundary\n");
            alpha = -alpha;
            cublasDaxpy(hBlas, n, &alpha, d, inc, s, inc);

            double std, sts, dtd, dsq;
            cublasDdot(hBlas, n, s, inc, d, inc, &std);
            cublasDdot(hBlas, n, s, inc, s, inc, &sts);
            cublasDdot(hBlas, n, d, inc, d, inc, &dtd);
            dsq = delta * delta;
            double rad = sqrt(std * std + dtd * (dsq - sts));
            if(std >= 0) {
                alpha = (dsq - sts) / (std + rad);
            } else {
                alpha = (rad - std) / dtd;
            }
            cublasDaxpy(hBlas, n, &alpha, d, inc, s, inc);
            alpha = -alpha;
            cublasDaxpy(hBlas, n, &alpha, Hd, inc, r, inc);
            break;
        }

        alpha = -alpha;
        cublasDaxpy(hBlas, n, &alpha, Hd, inc, r, inc);
        cublasDdot(hBlas, n, r, inc, r, inc, &rnewTrnew);
        beta = rnewTrnew / rTr;
        cublasDscal(hBlas, n, &beta, d, inc);
        cublasDaxpy(hBlas, n, &one, r, inc, d, inc);
        rTr = rnewTrnew;
    }

    cudaDeviceSynchronize();
    
    cudaMemcpy(cpu_s, s, sizeof(double) * n, cudaMemcpyDeviceToHost);
    cudaMemcpy(cpu_r, r, sizeof(double) * n, cudaMemcpyDeviceToHost);

    return cg_iter;
}
} // End namespace cuaccl_trcg.
